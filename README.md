# Trivy Config pre-commit hook

This repository provides a [pre-commit] hook that triggers a `trivy config .` when matching files are committed and ensures it is possible before allowing the commit to proceed. This can be used to validate Infrastructure projects are potentially valid.

## Usage

To utilize this hook add an entry to a `.pre-commit-config.yaml` file in the project root and ensure pre-commit is installed. This does require a working CDK environment to be already setup. So ensure the [cdk cli] is installed along with any other dependencies.

**Example:**

```yaml
repos:
  - repo: https://gitlab.com/sscc/pre-commit-trivy-config
    rev: v0.0.2
    hooks:
      - id: trivy-config
```

[pre-commit]: https://pre-commit.com/
[AWS CDK]: https://github.com/aws/aws-cdk
[cdk cli]: https://docs.aws.amazon.com/cdk/latest/guide/cli.html
